#!/bin/bash


SERVER=$1
NUMPROCS=$2
DATADIR=$3
RUNTIME=$4
INTERVAL=$5
OMIT=$6
WINDOW=$7
STARTPORT=$8

if [ $# -ne 8 ]
then
    echo "usage:SERVER NUMPROCS DATADIR RUNTIME INTERVAL OMIT WINDOW STARTPORT"
    exit 1
fi

#RUNTIME=60
#INTERVAL=1
#OMIT=10
#WINDOW=16K
#STARTPORT=6000


#for ((i=1; i<= $NUMPROCS; i++))
#do
#    port=$(( ${STARTPORT} + $i ))
#    iperf3 -c $SERVER --port $port --omit $OMIT -t $RUNTIME -i $INTERVAL -T "T: $SERVER, $RUNTIME, $i" --logfile "${DATADIR}/data_${SERVER}_${RUNTIME}_${i}" & 
#done

mkdir ${DATADIR}

for ((i=0; i< $NUMPROCS; i++))
do
    port=$(( ${STARTPORT}  ))
    logfile="${DATADIR}/data_${SERVER}_${RUNTIME}_${i}"
    rm -f ${logfile}
    cmd=( iperf3 -c ${SERVER} --port ${port} --omit ${OMIT} -t ${RUNTIME} -i ${INTERVAL}  --logfile "${DATADIR}/data_${SERVER}_${RUNTIME}_${i}" )
    echo "# ${cmd[@]}"
    echo "# ${cmd[@]}" > ${logfile}
    "${cmd[@]}"
done

# copy results out of container
tar czf ${DATADIR}_results.tar.gz ${DATADIR}
scp -o BatchMode=yes -o StrictHostKeyChecking=no -i katatest_id_rsa  ${DATADIR}_results.tar.gz cadeniyi@e113630-lin:/home/cadeniyi/data
