# Running benchmarks to compare kata-containers and runc containers


This repo containers the following:


* ping.sh - script to call ping to measure the round-trip ping time between the container and an IP address that could be a remote machine, the local machine or a container instance.
* perf.sh - script using iperf3 client to measure the available netowrk bandwidth between the container the the iperf3 server (running remotely, locally or within a separate container)
* perf_server.sh - script to run the iperf3 server within the container.

* Dockerfile for building a container image based on Alpine plus the iperf3 binary, ping.sh, perf.sh and perf_server.sh scripts

* run_all - a script that attempts to run ping and iperf3 tests. Runs all four cases: connection to a remote machine, connection to a local process, connection to a runc container and connection to a kata-container container.

The run_all script should be customised to set the IP addresses for the various instances of the remote machine/local machine and local containers.
