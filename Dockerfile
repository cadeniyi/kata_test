FROM alpine:3.13

RUN apk add iperf3 bash openssh-client tar
COPY perf.sh /perf.sh
COPY ping.sh /ping.sh
COPY perf_server.sh /perf_server.sh
COPY katatest_id_rsa /katatest_id_rsa

ENV TMPDIR=/dev/shm

ENTRYPOINT ["/bin/bash"]
