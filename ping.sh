#!/bin/bash

SERVER=$1
NUMPROCS=$2
DATADIR=$3
COUNT=$4

if [ $# -ne 4 ]
then
    echo "usage:SERVER NUMPROCS DATADIR COUNT"
    exit 1
fi

for ((i=0; i< $NUMPROCS; i++))
do
    logfile="${DATADIR}/ping_${SERVER}_${i}"
    rm -f ${logfile}
    cmd=( ping -c ${COUNT} ${SERVER} )
    echo "# ${cmd[@]}"
    echo "# ${cmd[@]}" > ${logfile}
    "${cmd[@]}" > ${logfile} 
done
